jQuery.noConflict();
jQuery(document).ready(function() {
	jQuery('a.lightbox').fancybox({
		arrows: true,
		helpers : {
			title: {
				type: 'over'
			},
			overlay : {
				opacity: 0.9,
				css : {
					'background-color' : '#fff'
				}
			}
		}
	});
});