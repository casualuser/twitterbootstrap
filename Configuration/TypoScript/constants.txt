/**
 * Constants for extension twitterbootstrap
 * @author Andreas Otto <andreas@otto-hanika.de>
 */

/**
 * Set layout type
 */
twitterbootstrap.layout.type = fixed

/**
 * Set brand name and link
 */
twitterbootstrap.brand.name = example.com
twitterbootstrap.brand.page = 73

/**
 * Set pages of mainnav
 */
twitterbootstrap.mainnav.pages = {$twitterbootstrap.brand.page}
twitterbootstrap.mainnav.special = directory

/**
 * Set pages of footer
 */
twitterbootstrap.footer.pages = 78

/**
 * Define image settings and enable lightbox functionality with fancybox
 */
styles {
	content {
		imgtext {
			maxW = 920
			maxWInText = 920
			linkWrap.width = 920
			linkWrap.lightboxEnabled = 1
			linkWrap.lightboxCssClass = lightbox
			linkWrap.lightboxRelAttribute = lightbox{field:uid}
			separateRows = 0
			addIntextClearer = 1
		}
	}
}
